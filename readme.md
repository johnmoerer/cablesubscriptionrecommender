# Cable Package Recommendation Engine
Adapted from a pySpark tutorial by Jose A Dianes on codementor.io 
The original tutorial was focused on a generic product recommendation system
It has been adapted to create a cable TV package subscription recommendation 
engine specifically targeted for Shaw Communications to recommend additional 
cable tv channel packages for a user based on channels that other customers 
with similar interests have subscribed to.
The data file used for training has customer ID and package ID pairs
(mocked or masked data; no actual customer IDs used) with a boolean subscribed
flag of 0 for false or 1 for true (subscribed). Subscription predictions 
are calculated between 0 and 1 and can be consumed as a recommendation strength
between 0 and 1.

## Where to start
* **generate_ratings.py** - Configure and run this script to generate a csv file with random sample data. Supports manual creation of up to 10 non-random user categories with hard-coded subscriptions to ensure that there is enough non-random data for the generated model to be meaningful.
* **calibrate-model.py** - This script will do multiple ALS training runs with different configurations and test the resulting model accuracy to determine optimal model configuration params. (Run with start_calibrate-model.sh)
* **server.py** - The primary application framework. (Run with start_server.sh)

