import os
from pyspark.mllib.recommendation import ALS

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Suppress pyspark debug messages
pyspark_log = logging.getLogger('pyspark')
pyspark_log.setLevel(logging.ERROR)
py4j_log = logging.getLogger('py4j')
py4j_log.setLevel(logging.ERROR)


class CableRecommendationEngine:
	
	# 
	# Main Spark recommendation engine class. 
	# Adapted from a pySpark tutorial by Jose A Dianes on codementor.io 
	# The original tutorial was focused on a generic product recommendation system
	# It has been adapted to create a cable TV package subscription recommendation 
	# engine specifically targeted for Shaw Communications to recommend additional 
	# cable tv channel packages for a user based on channels that other customers 
	# with similar interests have subscribed to.
	# The data file used for training has customer ID and package ID pairs
	# (mocked or masked data; no actual customer IDs used) with a boolean subscribed
	# flag of 0 for false or 1 for true (subscribed). Subscription predictions 
	# are calculated between 0 and 1 and can be consumed as a recommendation strength
	# between 0 and 1.
	

	def __train_model(self):
		# Train the ALS subscription prediction model with the configured dataset
		
		logger.info("Training the ALS subscription prediction model...")
		self.model = ALS.train(self.subscriptions_RDD, self.rank, seed=self.seed,
                               iterations=self.iterations, lambda_=self.regularization_parameter)
		logger.info("ALS subscription prediction model training complete")
		

	def get_recommendations_for_package_ids(self, user_id, package_ids):
        # Take a user_id and a list of package_ids, predict subscription interest 
        # (float between 0-1) for each package 
		
		requested_packages_RDD = self.sc.parallelize(package_ids).map(lambda x: (user_id, x))
		# Get predicted subscriptions
		subscriptions = self.__predict_subscriptions(requested_packages_RDD).collect()

		return subscriptions
    
	def get_top_recommendations(self, user_id, packages_count):
		# Recommends top packages, up to packages_count for user_id
		# that user_id is not already subscribed to
    	
		logger.debug("Get Top %s Recommendations for user %s" % (packages_count, user_id))
		
		# User is NOT subscribed to the following packages
		rdd1 = self.subscriptions_RDD.filter(lambda subscription: not subscription[0] == user_id)
		
		# get package recommendations for the following user, package combinations
		user_recommended_packages_RDD = rdd1.map(lambda x: (user_id, x[1])).distinct()
        
		# Get predicted subscription scores (0-1)	
		predictions_unsort = self.__predict_subscriptions(user_recommended_packages_RDD).map(lambda r: {"pkg_id": r[0],"pred_score": r[1][0],"pkg_name": r[1][1]})
		
		subscriptions_sort = predictions_unsort.takeOrdered(packages_count, key=lambda x: -x["pred_score"])
		
		logger.debug("returning recommendations")
		
		return subscriptions_sort
		
		

	def __predict_subscriptions(self, user_and_package_RDD):
		# Gets predictions for (userID, packageID) formatted RDD
		# Returns: an RDD with format (packageName, packagePrediction)
		logger.debug("predict_subscriptions")
		logger.debug(user_and_package_RDD.take(1))
		
		predicted_RDD = self.model.predictAll(user_and_package_RDD)
        
		logger.debug("predictAll complete")
		logger.debug(predicted_RDD.take(1))
        
		predicted_subscription_RDD = predicted_RDD.map(lambda x: (x.product, x.rating))
		
		logger.debug("map complete")
		logger.debug(predicted_subscription_RDD.take(1))
		
		predicted_subscription_name_RDD = \
			predicted_subscription_RDD.join(self.packages_RDD)
			
		logger.debug("join complete")
		logger.debug(predicted_subscription_name_RDD.take(1))
		
		#predicted_subscription_name_RDD = \
 		#	predicted_subscription_name_RDD.map(lambda r: (r[1][0][1], r[1][0][0], r[1][1]))
        
		return predicted_subscription_name_RDD

	def __init__(self, sc, dataset_path):
		# Init the recommendation engine given a Spark context and a dataset path

		logger.info("Starting up the Recommendation Engine: ")
		
		self.training_data_file_name = 'generated-user-subscriptions.csv'
		self.packages_info_file_name = 'packages.csv'

		self.sc = sc

		# Load subscriptions data for later use
		logger.info("Loading subscriptions data...")
		subscriptions_file_path = os.path.join(dataset_path, 'subscriptions.csv')
		subscriptions_raw_RDD = self.sc.textFile(subscriptions_file_path)
        
		# Remove header, cast IDs to int and cast subscription strength to float
		subscriptions_raw_data_header = subscriptions_raw_RDD.take(1)[0]
		self.subscriptions_RDD = subscriptions_raw_RDD.filter(lambda line: line!=subscriptions_raw_data_header)\
			.map(lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]),int(tokens[1]),float(tokens[2]))).cache()
            
		# Load packages data for later use
		logger.info("Loading Packages data...")
		packages_file_path = os.path.join(dataset_path, self.packages_info_file_name)
		packages_raw_RDD = self.sc.textFile(packages_file_path)
		packages_raw_data_header = packages_raw_RDD.take(1)[0]
		self.packages_RDD = packages_raw_RDD.filter(lambda line: line!=packages_raw_data_header)\
			.map(lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]),tokens[1])).cache()


		# Train the model using optimal training paramaters
		# See calibrate-model.py for model fit and calibration process
        
		self.rank = 32 # Optimal rank determined with calibrate-model.py
		self.seed = 5L
		self.iterations = 8
		self.regularization_parameter = 0.2
		self.__train_model() 
