import os, logging, math
from pyspark.mllib.recommendation import ALS
from pyspark import SparkContext, SparkConf
from pyspark.mllib.linalg.distributed import RowMatrix

# This script will load the sample user info and subscription data, 
# split it into a training set and test/validation set, try training
# multiple models using different training paramaters, and then test
# to see which models predict the test/validation set the best. The 
# result will be a set of training paramaters for use in training an
# actual model
 
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Suppress pyspark debug messages
pyspark_log = logging.getLogger('pyspark')
pyspark_log.setLevel(logging.ERROR)
py4j_log = logging.getLogger('py4j')
py4j_log.setLevel(logging.ERROR)

def init_spark_context():
    # load spark context
    conf = SparkConf().setAppName("cable_package_recommendation_server")
    sc = SparkContext(conf=conf)
 
    return sc

# Load user subscriptions file and strip out header row
#subs_file = 'user-subscriptions.csv'
subs_file = '../data/subscriptions.csv'
sc = init_spark_context()
	
raw_subscription_data = sc.textFile(subs_file)
subscription_header = raw_subscription_data.take(1)[0]

subscription_data = raw_subscription_data.filter(lambda line: line!=subscription_header).map(lambda line: line.split(",")).map(lambda c: (c[0], c[1], float(c[2]))).cache()

# Now split the sample set into training, validation and test sets, and strip subscription column from validation and test sets
training_data_RDD, validation_RDD, test_data_RDD = subscription_data.randomSplit([.6,.2,.2], seed=0L)
training_validation_RDD = validation_RDD.map(lambda x: (x[0], x[1]))
training_test_RDD = test_data_RDD.map(lambda x: (x[0], x[1]))

# Initialize training paramaters
seed = 5L
iterations = 8
regularization_parameter = 0.2
ranks = [4, 8, 12, 24]
errors = [0, 0, 0, 0]
err = 0
tolerance = 0.02
min_error = float('inf')
best_rank = -1
best_iteration_count = -1
 
# And then iterate through paramater sets to try training model with different parameters
for rank in ranks:
	logger.debug('Train model with rank %s' % rank)
	model = ALS.train(training_data_RDD, rank, seed=seed, iterations=iterations, lambda_=regularization_parameter)
	predictions = model.predictAll(training_validation_RDD).map(lambda r: ((r[0], r[1]), r[2]))
	
	# Compare the original validation dataset against the predicted data set and calculate Root Mean Square Error
	rates_and_preds = validation_RDD.map(lambda r: ((int(r[0]), int(r[1])), float(r[2]))).join(predictions)
	error = math.sqrt(rates_and_preds.map(lambda r: (r[1][0] - r[1][1])**2).mean())
	
	errors[err] = error
	err+=1
	
	logger.debug('For rank %s the RMSE is %s' % (rank, error))
	
	if error < min_error:
		min_error = error
		best_rank = rank
		
logger.debug('The best model was trained with rank %s' % best_rank)
 