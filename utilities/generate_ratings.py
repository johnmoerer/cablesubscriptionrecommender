import csv
from random import randint

num_users = 10000
num_products = 13  

with open('../data/subscriptions.csv', 'wb') as csvfile:
	filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
	filewriter.writerow(['userID', 'packageID', 'subscription'])
	
	for u in xrange(0, num_users):
		for p in xrange(0, num_products):
			
			# Start with base random subscription info where
			# 1 = subscribed, 0 = not subscribed
			subscription_flag = randint(0,1)
			
			# To make subscription data more realistic, divide into
			# 10 TYPES of users by using mod 10.
			# 	1 = Cord-cutter sports fans. Subscribe (at least) package 0, 9, 12
			#	2 = Sports & news fan. Subscribe (at least) package 1, 6, 7, 9, 12
			#	3 = News junkie. Subscribe (at least) package 1, 6, 7, 11
			#	4 = Family friendly. Subscribe (at least) package 1, 2, 3, 4, 9
			#	5 = Random large package. Subscribe (at least) package 1
			#	6 = Movie buff. Subscribe (at least) package 0, 2, 8, 10
			#	7 = Random medium package. Subscribe (at least) package 0
			#	8 = Full random
			#	9 = Full random
			#	0 = Full random
			
			override_mod = u % 10
			if (override_mod == 1 and p in [0,9]):
				subscription_flag = 1
			elif (override_mod == 2 and p in [1,6,7,9,12]):
				subscription_flag = 1
			elif (override_mod == 3 and p in [1,6,7,11]):
				subscription_flag = 1
			elif (override_mod == 4 and p in [1,2,3,4,9]):
				subscription_flag = 1
			elif (override_mod == 5 and p in [1]):
				subscription_flag = 1
			elif (override_mod == 6 and p in [0,2,8,10]):
				subscription_flag = 1
			elif (override_mod == 7 and p in [0]):
				subscription_flag = 1
			elif (override_mod == 8 and p in [2,8]):
				subscription_flag = 1
			elif (override_mod == 9 and p in [12,7]):
				subscription_flag = 1
				
			filewriter.writerow(['%s' % u, '%s' % p, subscription_flag])