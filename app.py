from flask import Blueprint
from flask import Flask, request
import json, logging
from engine import CableRecommendationEngine

#
# Flask web application establishes two web services to recommend
# Shaw cable packages for a customer (given customer ID)
# Top recommendations service returns the top n package recommendations
# Recommendations service returns a recommendation score (between 0-1)
# for a given user ID and package ID
# 
# See packages.csv for package IDs and names
# See generate_ratings.py for a description of the generated subscription data file
#

main = Blueprint('main', __name__)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
  
@main.route("/<int:user_id>/recommendations/top/<int:count>", methods=["GET"])
def top_recommendations(user_id, count):
    logger.debug("User %s TOP recommendations requested", user_id)
    top_recommendations = recommendation_engine.get_top_recommendations(user_id,count)
    return json.dumps(top_recommendations)
 
@main.route("/<int:user_id>/recommendations/<int:package_id>", methods=["GET"])
def package_recommendations(user_id, package_id):
    logger.debug("User %s recommendations requested for package %s", user_id, package_id)
    ratings = recommendation_engine.get_recommendations_for_package_ids(user_id, [package_id])
    return json.dumps(ratings)
 
 
def create_app(spark_context, dataset_path):
    global recommendation_engine 

    recommendation_engine = CableRecommendationEngine(spark_context, dataset_path)    
    
    app = Flask(__name__)
    app.register_blueprint(main)
    return app 
